import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";

const Table = ({ type, title }) => {
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch({type});
  }, []);
  
  return (
    <div>
      {title}
    </div>
  );
};

export default Table;