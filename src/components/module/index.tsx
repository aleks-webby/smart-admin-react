import React, {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Routes,
  Route,
} from "react-router-dom";
import Table from "../Table";

const Module = ({ type, title }) => {
  
  return (
    <div>
      <Routes>
        <Route path={`/${type}`} element={<Table type={type} title={title} />} />
      </Routes>
    </div>
  );
};

export default Module;