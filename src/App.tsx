import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import logo from './logo.svg';
import './App.css';
import Dashboard from "./components/Dashboard";
import Module from "./components/module";

const modules = [
  'service'
];

const App = () => {
  const dispatch = useDispatch();
  
  useEffect(() => {
  
  }, []);
  
  return (
    <Dashboard>
      <Module type="services" title="Услуги" />
      <Module type="users" title="Пользователи" />
    </Dashboard>
  );
}


export { modules };
export default App;
