import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import Reducer from "./reducer";

export function configureStore(InitialState?: any) {
  const Store = createStore(
    Reducer,
    InitialState,
    composeWithDevTools(applyMiddleware(thunk))
  );
  return Store;
}
