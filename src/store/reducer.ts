import { combineReducers } from "redux";

import adminModule from "./admin-module/reducer";

const Reducers = combineReducers({
  adminModule,
});

export default Reducers;
