import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import * as React from "react";

const Copyright = () => {
  return (
    <Typography variant="body2" color="text.secondary" align="center" sx={{ pt: 4 }} >
      {'Copyright © '}
      <Link color="inherit" href="https://smartigy.ru/">
        Smartigy
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default Copyright;